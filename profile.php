<? $fli = 1; ?>
<? include('h.php'); ?>

	<div class="jumbotron">
		<div class="container">
			<h1>Profile</h1>
			<p class="lead">Update your profile!</p>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="span6 offset3">
				<div class="well">
					<form class="form-horizontal" id="profileForm">
						<legend>Profile</legend>
						
						<div class="alert alert-success" style="display: none;" id="statusMsg"></div>
						
						<? if($me->fpwc): ?>
						<div class="alert" id="fpwc">You've been requested to change your password. Sorry about the inconvenience! Change your password to continue.</div>
						<? endif; ?>
						
						<div class="control-group">
							<label class="control-label">Full Name:</label>
							<div class="controls">
								<input type="text" placeholder="username" name="username" value="<?= $me->username; ?>" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Email:</label>
							<div class="controls" id="email">
								<input type="email" placeholder="email" name="email" value="<?= $me->email; ?>" />
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">Avatar:</label>
							<div class="controls">
								<img src="http://www.gravatar.com/avatar/<?= md5($me->email); ?>.png?s=30" />
								<span class="help-inline">Update your Avatar at <a href="http://gravatar.com" target="_blank">Gravatar.com</a></span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">New Password?</label>
							<div class="controls">
								<input type="checkbox" name="newPw" id="newPw"<? if($me->fpwc) echo ' checked="checked"'; ?> />
							</div>
						</div>
						
						<div id="pwFields" <? if(!$me->fpwc) echo 'style="display: none;"'; ?>>
						
							<div class="control-group" id="pwField1">
								<label class="control-label">Password:</label>
								<div class="controls">
									<input type="password" name="pw" id="pw" />
								</div>
							</div>
							
							<div class="control-group" id="pwField2">
								<label class="control-label">Repeat:</label>
								<div class="controls">
									<input type="password" name="pw2" id="pw2" />
								</div>
							</div>
							
						</div>
						
						<input type="hidden" name="action" value="profile" />
						
						<button type="submit" class="btn btn-primary btn-block">Update Profile</button>
						
					</form>
				</div>
			</div>
		</div>

<? $custom['js'][] = "/assets/js/profile.js"; ?>
<? include('f.php'); ?>