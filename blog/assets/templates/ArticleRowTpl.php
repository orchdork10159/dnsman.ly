<div class="well">    
	<h1><a href="[[~[[+id]]]]">[[+pagetitle]]</a>
		<small>[[+publishedon:strtotime:date=`%b %d, %Y`]]</small>
	</h1>
	<hr />
	
	<div class="entry">
		<p class="clearfix">
			<a href="[[~[[+id]]]]"><img src="[[+tv.articleImg:phpthumbof=`h=120&w=120&zc=1`]]" class="img-rounded pull-left" style="margin: 5px;" /></a>
			[[+introtext:default=`[[+content:stripTags:ellipsis=`800`]]`]]</p>
	</div>
	<div class="clearfix">
		<div class="btn-group pull-left">
			<button type="button" class="btn btn-small btn-inverse"><i class="icon-tags icon-white"></i></button>
			[[!tolinks? &items=`[[+tv.articlestags]]` &tpl=`tagButtonGroup` &target=`4` &tagKey=`articlestags`]]
		</div>
		<div class="btn-group pull-right">
			<a class="btn btn-info btn-mini" href="[[~[[+id]]]]#comments">Comments (<fb:comments-count href="[[++site_url]][[~[[+id]]]]" /></fb:comments-count>)</a>
			<a class="btn btn-info btn-mini" href="[[~[[+id]]]]">Read More ></a>
		</div>
	</div>
</div>