[[!include? &file=`h.php`]]

	<div class="jumbotron">
		<div class="container">
			<h1>Blog</h1>
			<p class="lead">Stay up to date!</p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li class="active">Blog</li>
		</ul>
		
		<div class="row">
			<div class="span3">
				<div class="well" data-spy="affix" data-offset-top="250" style="padding: 8px 0;">
					<ul class="nav nav-list">
						<li class="nav-header">Common Tags</li>
							[[!tagLister? &tv=`articlestags` &tpl=`tagList` &target=`4`]]
					</ul>
				</div>
			</div>
			<div class="span7">
				[[*content]]
			</div>
			<div class="span2 hidden-phone">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3331705717583360";
				/* DNSManPro Side */
				google_ad_slot = "9392018718";
				google_ad_width = 120;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>
		</div>
	
[[!include? &file=`f.php`]]