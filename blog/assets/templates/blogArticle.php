[[!include? &file=`h.php` &isBlog=`1`]]
	
	<div class="jumbotron">
		<div class="container">
			<h1>Blog</h1>
			<p class="lead">[[*pagetitle]]</p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="/blog">Blog</a> <span class="divider">/</span>
			</li>
			<li class="active">[[*pagetitle]]</li>
		</ul>
	<!-- 	<hr /> -->
		
		<div class="row">
			<div class="span3">
				<div class="well noshadow" data-spy="affix" data-offset-top="250" style="padding: 8px 0;">
					<ul class="nav nav-list">
						<li>
							<a href="[[*articleImg]]" class="fancybox"><img src="[[*articleImg:phpthumbof=`w=270`]]" class="img-rounded" /></a>
						</li>
						<li class="divider"></li>
						<li><a href="#comments">Comments (<fb:comments-count href="[[++site_url]][[~[[*id]]]]"></fb:comments-count>)</a></li>
						<li class="divider"></li>
						<li class="nav-header">Common Tags</li>
							[[!tagLister? &tv=`articlestags` &tpl=`tagList` &target=`4`]]
					</ul>
				</div>
			</div>
			<div class="span7">
				<div class="well">
					<div class="fb-like pull-right" data-send="[[++site_url]][[~[[*id]]]]" data-layout="box_count" data-width="50" data-show-faces="false"></div>
					<h1>[[*pagetitle]]
						<small>[[*publishedon:strtotime:date=`%b %d, %Y`]]</small>
					</h1>
					<div class="btn-group">
						<button type="button" class="btn btn-small btn-inverse"><i class="icon-tags icon-white"></i></button>
						[[!tolinks? &items=`[[*articlestags]]` &tpl=`tagButtonGroup` &target=`4` &tagKey=`articlestags`]]
					</div>
				</div>
	<!--
				<div class="well">
					<img src="[[*articleImg:phpthumbof=`w=670`]]" class="img-rounded" style="width: 100%;" />
				</div>
	-->
				<div class="well">
					[[*content]]
				</div>
				<div class="well">
					<a name="comments"></a>
					<div class="fb-comments" data-href="[[++site_url]][[~[[*id]]]]" data-num-posts="2" data-width="470"></div>
				</div>
			</div>
			<div class="span2 hidden-phone">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3331705717583360";
				/* DNSManPro Side */
				google_ad_slot = "9392018718";
				google_ad_width = 120;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>
		</div>
	
[[!include? &file=`f.php`]]