<?php  return array (
  'properties' => 
  array (
    'namespace' => '',
    'limit' => 10,
    'offset' => 0,
    'page' => 1,
    'pageVarKey' => 'page',
    'totalVar' => 'total',
    'pageLimit' => 5,
    'elementClass' => 'modSnippet',
    'pageNavVar' => 'page.nav',
    'pageNavTpl' => '<li[[+classes]]><a[[+classes]][[+title]] href="[[+href]]">[[+pageNo]]</a></li>',
    'pageNavOuterTpl' => '[[+first]][[+prev]][[+pages]][[+next]][[+last]]',
    'pageActiveTpl' => '<li[[+activeClasses]]><a class="active"[[+title]] href="[[+href]]">[[+pageNo]]</a></li>',
    'pageFirstTpl' => '<li class="control"><a[[+classes]][[+title]] href="[[+href]]">First</a></li>',
    'pageLastTpl' => '<li class="control"><a[[+classes]][[+title]] href="[[+href]]">Last</a></li>',
    'pagePrevTpl' => '<li class="control"><a[[+classes]][[+title]] href="[[+href]]">&lt;&lt;</a></li>',
    'pageNextTpl' => '<li class="control"><a[[+classes]][[+title]] href="[[+href]]">&gt;&gt;</a></li>',
    'cache' => true,
    'cache_key' => 'resource',
    'cache_handler' => 'xPDOFileCache',
    'cache_expires' => 0,
    'pageNavScheme' => '',
    'element' => 'getArchives',
    'makeArchive' => '0',
    'parents' => '2',
    'where' => '{"class_key":"Article"}',
    'showHidden' => '1',
    'includeContent' => '1',
    'includeTVs' => '1',
    'includeTVsList' => '',
    'processTVs' => '1',
    'processTVsList' => '',
    'tagKey' => 'articlestags',
    'tagSearchType' => 'contains',
    'sortby' => 'publishedon',
    'sortdir' => 'DESC',
    'tpl' => 'ArticleRowTpl',
    'total' => 3,
    'pageOneLimit' => 10,
    'actualLimit' => 10,
    'toPlaceholder' => '',
    'cachePageKey' => 'web/resources/2/1/d41d8cd98f00b204e9800998ecf8427e',
    'cacheOptions' => 
    array (
      'cache_key' => 'resource',
      'cache_handler' => 'xPDOFileCache',
      'cache_expires' => 0,
    ),
    'qs' => 
    array (
    ),
    'pageCount' => 1,
    'firstItem' => 1,
    'lastItem' => 3,
    'pageUrl' => '/blog/',
  ),
  'output' => '<div class="well">    
	<h1><a href="/blog/dnsman.ly-goes-secure/">DNSMan.ly Goes Secure</a>
		<small>Sep 06, 2012</small>
	</h1>
	<hr />
	
	<div class="entry">
		<p class="clearfix">
			<a href="/blog/dnsman.ly-goes-secure/"><img src="/blog/assets/components/phpthumbof/cache/ssl.8643d3e3e449dc74c002a03ddf30563f2.png" class="img-rounded pull-left" style="margin: 5px;" /></a>
			Many members were asking for SSL security to be installed on DNSMan.ly. We have made it happen!
Although our Certificate isn\'t trusted by every browser and OS out there, DNSMan.ly is now completely encrypted using 256-bit encryption. We are working to afford a more trusted certificate, but for now, we\'re safe!</p>
	</div>
	<div class="clearfix">
		<div class="btn-group pull-left">
			<button type="button" class="btn btn-small btn-inverse"><i class="icon-tags icon-white"></i></button>
			[[!tolinks? &items=`` &tpl=`tagButtonGroup` &target=`4` &tagKey=`articlestags`]]
		</div>
		<div class="btn-group pull-right">
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-goes-secure/#comments">Comments (<fb:comments-count href="http://dnsman.ly/blog/dnsman.ly-goes-secure/" /></fb:comments-count>)</a>
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-goes-secure/">Read More ></a>
		</div>
	</div>
</div>
<div class="well">    
	<h1><a href="/blog/dnsman.ly-interface-comparison/">DNSMan.ly Interface Comparison</a>
		<small>Aug 27, 2012</small>
	</h1>
	<hr />
	
	<div class="entry">
		<p class="clearfix">
			<a href="/blog/dnsman.ly-interface-comparison/"><img src="/blog/assets/components/phpthumbof/cache/gd_fields.8643d3e3e449dc74c002a03ddf30563f2.png" class="img-rounded pull-left" style="margin: 5px;" /></a>
			The Domain Name System is a relatively new service (along with the internet), having been invented in 1982. Since its introduction into streamline internet use, companies have had to find a way to allow more novice users to manage their DNS zones without using a command-line text editor like Nano or Vim.Â 
GoDaddy DNS Manager
My biggest issue with DNS has always been the process of editing my DNS Zone. I used to host my DNS with GoDaddy. Here\'s a preview of what GoDaddy\'s DNS Manager looks like.

Common tasks with DNS Zone editing can include adding records, deleting records, and editing records (duh!). Let\'s take a look at what it would take at GoDaddy to get this accomplished.

First, I had to find my DNS Zone, which is obviously a task that would need to be completed at any DNS manager.
Once&#8230;</p>
	</div>
	<div class="clearfix">
		<div class="btn-group pull-left">
			<button type="button" class="btn btn-small btn-inverse"><i class="icon-tags icon-white"></i></button>
			[[!tolinks? &items=`dnsmanly, godaddy, rackspace, rsc` &tpl=`tagButtonGroup` &target=`4` &tagKey=`articlestags`]]
		</div>
		<div class="btn-group pull-right">
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-interface-comparison/#comments">Comments (<fb:comments-count href="http://dnsman.ly/blog/dnsman.ly-interface-comparison/" /></fb:comments-count>)</a>
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-interface-comparison/">Read More ></a>
		</div>
	</div>
</div>
<div class="well">    
	<h1><a href="/blog/dnsman.ly-featured-on-betali.st/">DNSMan.ly Featured on Betali.st</a>
		<small>Aug 26, 2012</small>
	</h1>
	<hr />
	
	<div class="entry">
		<p class="clearfix">
			<a href="/blog/dnsman.ly-featured-on-betali.st/"><img src="/blog/assets/components/phpthumbof/cache/ad.84fe7b744052f59d82284a2198eaad5c2.jpg" class="img-rounded pull-left" style="margin: 5px;" /></a>
			DNSMan.ly was recently featured on Betali.st, introducing this great tool to the curious web population!
Check out the listing!</p>
	</div>
	<div class="clearfix">
		<div class="btn-group pull-left">
			<button type="button" class="btn btn-small btn-inverse"><i class="icon-tags icon-white"></i></button>
			[[!tolinks? &items=`betalist,feature,dnsmanly` &tpl=`tagButtonGroup` &target=`4` &tagKey=`articlestags`]]
		</div>
		<div class="btn-group pull-right">
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-featured-on-betali.st/#comments">Comments (<fb:comments-count href="http://dnsman.ly/blog/dnsman.ly-featured-on-betali.st/" /></fb:comments-count>)</a>
			<a class="btn btn-info btn-mini" href="/blog/dnsman.ly-featured-on-betali.st/">Read More ></a>
		</div>
	</div>
</div>',
);