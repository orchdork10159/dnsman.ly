<?php  return array (
  1 => 
  array (
    'basePath' => '../',
    'basePathRelative' => true,
    'baseUrl' => '/',
    'baseUrlRelative' => false,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Filesystem',
    'description' => '',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => '../',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => '/',
        'lexicon' => 'core:source',
      ),
      'baseUrlRelative' => 
      array (
        'name' => 'baseUrlRelative',
        'desc' => 'prop_file.baseUrlRelative_desc',
        'type' => 'combo-boolean',
        'options' => 
        array (
        ),
        'value' => false,
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 1,
    'object_class' => 'modTemplateVar',
    'object' => 1,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
  2 => 
  array (
    'basePath' => '../',
    'basePathRelative' => true,
    'baseUrl' => '/',
    'baseUrlRelative' => false,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Filesystem',
    'description' => '',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => '../',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => '/',
        'lexicon' => 'core:source',
      ),
      'baseUrlRelative' => 
      array (
        'name' => 'baseUrlRelative',
        'desc' => 'prop_file.baseUrlRelative_desc',
        'type' => 'combo-boolean',
        'options' => 
        array (
        ),
        'value' => false,
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 1,
    'object_class' => 'modTemplateVar',
    'object' => 2,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
);