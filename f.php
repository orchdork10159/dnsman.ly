    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js"></script>
    <script src="/assets/js/jquery.tablesorter.min.js"></script>
    <script src="/assets/js/jquery.shadow.js"></script>
    <script src="/assets/fancybox/jquery.fancybox.pack.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap-tooltip.js"></script>
    <script src="/assets/js/bootstrap-popover.js"></script>
    <script src="/assets/js/custom.js"></script>
    <!-- Custom JS --><?php
    	foreach($custom['js'] as $js)
    		echo "
    <script src='".$js."'></script>";
    ?>
    <? if($lockdown): ?>
    <script type="text/javascript">
    	$(function(){lockDown();});
    </script>
    <? endif; ?>
    
    <div class="alert alert-info" style="text-align: center;">
    	<strong>Give us your feedback!</strong>
		<link href="https://d3jyn100am7dxp.cloudfront.net/assets/widget_embed.cssgz?1345614819" media="screen" rel="stylesheet" type="text/css" />
		<!--If you already have fancybox on the page this script tag should be omitted-->
		<script src="https://d3jyn100am7dxp.cloudfront.net/assets/widget_embed_libraries.jsgz?1345614820" type="text/javascript"></script>

        <script>
                
                // ********************************************************************************
                // This needs to be placed in the document body where you want the widget to render
                // ********************************************************************************
                
                new DESK.Widget({ 
                        version: 1, 
                        site: 'support.dnsman.ly', 
                        port: '80', 
                        type: 'email', 
                        displayMode: 1,  //0 for popup, 1 for lightbox
                        features: {  
                        },  
                        fields: { 
                                ticket: { 
                                        // desc: '',
                // labels_new: '',
                // priority: '',
                // subject: ''
                                }, 
                                interaction: { 
                                        // email: '',
                // name: ''
                                }, 
                                email: { 
                                        //subject: '', 
                                        //body: '' 
                                }, 
                                customer: { 
                                        // company: '',
                // desc: '',
                // first_name: '',
                // last_name: '',
                // locale_code: '',
                // title: ''
                                } 
                        } 
                }).render();  
        </script>
    </div>

			<hr />
			<footer>
				<p style="float: left;">&copy; DNSMan.ly 2012 - <a href="http://facebook.com/dnsmanly" target="_blank"><img src="/i/fb.png"></a><a href="http://twitter.com/dnsmanly" target="_blank"><img src="/i/tw.png"></a></p>
				<div class="fb-like" data-href="https://www.facebook.com/dnsmanly" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false" data-font="lucida grande" style="float: right;"></div>
				<? if(!$noad): ?>
					<div style="padding-top: 50px;text-align: center;" class="hidden-phone">
						<script type="text/javascript"><!--
						google_ad_client = "ca-pub-3331705717583360";
						/* DNSManPro */
						google_ad_slot = "6405706959";
						google_ad_width = 728;
						google_ad_height = 90;
						//-->
						</script>
						<script type="text/javascript"
						src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
						</script>
					</div>
				<? endif; ?>
			</footer>
    	</div>
    </div>
   
</div> <!-- /container -->
<? if($me->id): ?>
<script id="IntercomSettingsScriptTag">
  window.intercomSettings = {
    email: "<?= $me->email; ?>",
    user_hash: "<?php echo hash_hmac("sha256", $me->email, "wR3wPabiEu7JWqiBH9SrzM-Ah-ogQqn0_t3bmtBe"); ?>",
    created_at: <?php echo strtotime($me->dateCreated); ?>,
    app_id: "5b48d3eff821c013043cf47540839a56b00fadf2",
    numAccounts: <?php echo count($me->getAccounts()); ?>
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}};})()</script>
<? endif; ?>
  </body>
</html>