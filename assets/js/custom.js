$(function(){

	$(".well, ins, table").shadow('lifted');
	$(".noshadow").removeClass("jquery-shadow");
	
	$(".fancybox").fancybox();
		
	$("#loginForm").submit(function(){
	
		var elem = $(this);

		$.post("/login",
			elem.serialize(),
			function(data){
				if(data.success == true)
					window.location.href = "/dashboard";
				else
					elem.effect('shake', {'times': 2}, 100);
			},
			"json");

		return false;
	
	});
	
	$("#logout").click(function(){
		
		$.post("/logout",
			null,
			function(data){
				window.location.href = "/";
			});
		
	});
	
});