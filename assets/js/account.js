function lockDown() {
	
	$("ul.breadcrumb").css({"zIndex":1041,"position":"relative"});
	$("#editAcc").removeClass("jquery-shadow jquery-shadow-lifted").css({"zIndex":1041,"position":"relative"});
	$(".modal-backdrop").fadeIn();
	$("#editAcc").slideDown();
	$("#editAccSummon").parent().addClass('active');
	$("#editAccCancel").remove();
	$("#invalidAlert").slideDown();
}

$(function(){

	$("#addForm").submit(function(){
		var myForm = $(this);
		var dataToSubmit = myForm.serialize();
		var formData = myForm.serializeArray();
		var request = new Object();
		
		for(x=0;x<formData.length;x++) {
			request[formData[x]['name']] = formData[x]['value'];
		}
		
		$("tfoot input[name=name]").attr('disabled', 'disabled');
		$("tfoot button.addDomainSubmit").attr('disabled', 'disabled').html("<i class='icon-refresh icon-white'></i>");
	
		$.post(
			"/api.php",
			dataToSubmit,
			function(data){
				if(data['status'] == "COMPLETED") {
					if(request['hosted'] == 1)
						var accUrl = 'hosted';
					else
						var accUrl = request['accountID'];
					var newRow = "\
					<tr>\
						<td><a href='/dashboard/account/"+accUrl+"/domain/"+data['newId']+"'>"+request['name']+"</a></td>\
						<td>\
						</td>\
						<td style='text-align: center;'>\
							<input type='checkbox' name='domainID[]' value='"+data['newId']+"' form='delForm'>\
						</td>\
					</tr>";
					$("tbody#domainsTable").append(newRow);
					
					$("tfoot input[name=name]").removeAttr('disabled').val("");
					$("tfoot button.addDomainSubmit").removeAttr('disabled').html("<i class='icon-plus icon-white'></i>");
				}
				else {
					$("tfoot td.addDomainSubmit").popover({
						"placement": "top",
						"trigger": "manual",
						"title": "Error",
						"content": "You've either entered an invalid domain name, or a domain already used by another user."
					}).popover('show');
					setTimeout(function(){$("tfoot td.addDomainSubmit").popover('hide');}, 5000);
					$("tfoot input[name=name]").removeAttr('disabled');
					$("tfoot button.addDomainSubmit").removeAttr('disabled').html("<i class='icon-plus icon-white'></i>");
				}
			},
			"json"
		);
				
		return false;
	});
	
	$("#delForm").submit(function(){
		var myForm = $(this);
		var dataToSubmit = myForm.serialize();
		var formData = myForm.serializeArray();
		var request = new Object();
		
		for(x=0;x<formData.length;x++) {
			request[formData[x]['name']] = formData[x]['value'];
		}
		
		$("tfoot td.delDomainSubmit").popover({
			"placement": "left",
			"trigger": "manual",
			"title": "Deleting",
			"content": "We're trying to delete your domains. This takes some time, so please be patient."
		}).popover('show');
		$("tbody :checkbox").attr('disabled', 'disabled');
		$("tfoot button.delDomainSubmit").attr('disabled', 'disabled').html("<i class='icon-refresh icon-white'></i>");
		
		$.post(
			"/api.php",
			dataToSubmit,
			function(data){
				if(data['status'] == "COMPLETED") {
					$("table :checked").each(function(){
						$(this).parent().parent().fadeOut().remove();
					});
					$("tfoot td.delDomainSubmit").popover('hide');
					$("tbody :checkbox").removeAttr('disabled');
					$("tfoot button.delDomainSubmit").removeAttr('disabled').html("<i class='icon-trash icon-white'></i>");					
				}
				else if(data['status'] == "RUNNING") {
					$("tfoot td.delDomainSubmit").popover('hide');
					$("tfoot button.delDomainSubmit i").popover({
						"placement": "left",
						"trigger": "manual",
						"title": "Still Running...",
						"content": "We're still trying to get your domains deleted, but it's taking a while. <a href=''>Reload the page</a> and check back in a few minutes to see the progress."
					}).popover('show');
				}
				else {
					$("tfoot td.delDomainSubmit").popover('hide');
					$("tfoot button.delDomainSubmit").popover({
						"placement": "left",
						"trigger": "manual",
						"title": "Error",
						"content": "An error has occurred. Some of your changes may have not been saved. Please <a href=''>reload the page</a> and try again."
					}).popover('show').html("<i class='icon-info-sign icon-white'></i>");
				}
			},
			"json"
		);
	
		return false;
	});

});