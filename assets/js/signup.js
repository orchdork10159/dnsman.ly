$(function(){

	$("#signupEmail").change(function(){
		var email = $.md5($(this).val());
		$("#signupAvatar").attr("src", "http://gravatar.com/avatar/"+email+".png?s=30");
	});

	$("#signup").submit(function(){
		var myForm = $(this);
		var dataToSubmit = myForm.serialize();
		var error = false;
		
		$(".error").removeClass("error").find("span").each(function(){$(this).remove();});
		
		if($("#pw").val() != $("#pw2").val())
			error = "match";
		if($("#pw").val().length < 7)
			error = "length";

		switch(error) {
			case "match":
				$("#pwField2").addClass("error").find(".controls").append("<span class='help-block'>Passowrds to not match!</span>");
			break;
			case "length":
				$("#pwField1").addClass("error").find(".controls").append("<span class='help-block'>Your password must be at least 7 characters.</span>");
			break;
			default:
				$("input, button", myForm).attr("disabled", "disabled");

				$.post(
					"/api.php",
					dataToSubmit,
					function(data){
						switch(data['status']) {
							case "email":
								$("#signupEmail").parent().append("<span class='help-block'>This email already exists. Forgot your password?</span>").parent().addClass('error');
								$("input, button", myForm).removeAttr('disabled');
							break;
							case "success":
								$("#signupFormContents").slideUp();
								$("#signupSuccess").slideDown();
							break;
							default:
								$("#signupError").slideDown();
							break;
						}
					},
					"json"
				);
			break;
		}
		
	
		return false;
	});
	
});