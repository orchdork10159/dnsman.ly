$(function(){

	$("#installPluginAccount").change(function(){
		$("#installPluginDomain").attr('disabled','disabled').html('<option value="">Loading Domains...</option>');
		$("#installPluginSubmit").attr('disabled','disabled');
		$("#installPluginDomainName").val("");
		$.post(
			"/api.php",
			"action=getDomains&accountID="+$(this).val()+"&hosted="+$("option:selected", this).attr('hosted'),
			function(data){
				$("#installPluginDomain").removeAttr('disabled').html(data['html']);
			},
			"json"
		);
	});
	
	$("#installPluginDomain").change(function(){
		if($(this).val() != "") {
			$("#installPluginSubmit").removeAttr("disabled");
			$("#installPluginDomainName").val($("option[value="+$(this).val()+"]").text());
		}
		else {
			$("#installPluginSubmit").attr("disabled", "disabled");
			$("#installPluginDomainName").val("");
		}
	});
	
	$("#installPlugin").submit(function(){
		var myForm = $(this);
		var dataToSend = myForm.serialize();
		
		$("select, button", myForm).attr('disabled','disabled');
		$("#installPluginSubmit").html("Install <i class='icon-refresh icon-white'></i>");
		
		$.post(
			"/api.php",
			dataToSend+"&hosted="+$("option:selected", this).attr('hosted'),
			function(data){
				switch(data['status']) {
					case "COMPLETED":
						if($("option:selected", this).attr('hosted'))
							document.location="/dashboard/account/hosted/domain/"+$("#installPluginDomain").val()+"?pluginInstall=1";
						else
							document.location="/dashboard/account/"+$("#installPluginAccount").val()+"/domain/"+$("#installPluginDomain").val()+"?pluginInstall=1";
					break;
					default:
						$("#installPluginSubmit").popover({
							"trigger": "manual",
							"title": "Unknown Error",
							"content": "An unknown error has occurred. Please try again later."
						}).popover('show');
				}
			},
			"json"
		);
		
		
		return false;
	});

});