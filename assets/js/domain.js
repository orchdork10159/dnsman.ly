$(function(){
	
	$(".sort-records").tablesorter({
		"headers":{
			"4":{ //Actions
				"sorter":false
			},
			"5":{ //Delete
				"sorter":false
			}
		}
	});
	
	$(".selectPlugin").click(function(){
		var plugin = $(this).attr('plugin');
		$("#domainTable :checkbox").removeAttr('checked');
		$("#domainTable tr").each(function(){
			if($(this).attr('plugin') == plugin) {
				$(":checkbox", $(this)).attr('checked','checked');
			}
		});
	});
	
	//Edit Record Actions
	function createEditRecordClickListener() {
		$("td.editable").click(function(){
			$("td.editable").unbind('click');
			var myTr = $(this).parent();
			
			//Sets up Editable Fields			
			$("td.editable[name=name]", myTr).attr("oldVal", $("td.editable[name=name]", myTr).html()).html("<input type='text' value='"+$("td.editable[name=name] span", myTr).text()+"' name='name' form='editForm' /><span class='add-on'>."+myTr.parent().attr('domainName')+"</span>");
			$("td.editable[name=ttl]", myTr).attr("oldVal", $("td.editable[name=ttl]", myTr).html()).html("<input type='number' min='300' class='input-mini' value='"+$("td.editable[name=ttl]", myTr).text()+"' name='ttl' form='editForm' />");
			if(myTr.attr('recordType') == "MX") {
				var n = $("td.editable[name=data]", myTr).text().indexOf(" ");
				var priority = $("td.editable[name=data]", myTr).text().substring(0,n);
				var data = $("td.editable[name=data]", myTr).text().substring(n+2);
			
				$("td.editable[name=data]", myTr).attr("oldVal", $("td.editable[name=data]", myTr).html()).html("<input type='number' class='input-mini' min='0' max='65535' value='"+priority+"' name='priority' form='editForm' /><input type='text' value='"+data+"' name='data' form='editForm' />");
			}
			else
				$("td.editable[name=data]", myTr).attr("oldVal", $("td.editable[name=data]", myTr).html()).html("<input type='text' value='"+$("td.editable[name=data]", myTr).text()+"' name='data' form='editForm' />");
			//Sets up Uneditable Fields
			$("td.recordActionsRow", myTr).attr("oldVal", $("td.recordActionsRow", myTr).html()).html("<button type='submit' class='btn btn-success' form='editForm'><i class='icon-ok icon-white'></i></button>");
			$("td.recordDeleteRow", myTr).attr("oldVal", $("td.recordDeleteRow", myTr).html()).html("<button class='btn btn-warning' id='editRecordCancel'><i class='icon-remove icon-white'></i></button>");
			$("#editForm input[name=recordID]").val(myTr.attr("recordID"));
			$("input:first", myTr).focus();
			
			createEditRecordListeners();
		
		});
	}
	createEditRecordClickListener();
	
	function createEditRecordListeners() {
		$("#editRecordCancel").click(function(){
			$("#editRecordCancel").unbind('click');
			var myTr = $(this).parent().parent();
			$("td[oldVal]", myTr).each(function(){
				$(this).html($(this).attr("oldVal")).removeAttr("oldVal");
			});
			$("td.recordActionsRow", myTr).popover('hide');
			createEditRecordClickListener();
		});
		$("#editForm").submit(function(){
			$("#editForm").unbind('submit');
			var myForm = $(this);
			var dataToSend = myForm.serialize();
			var formData = myForm.serializeArray();
			var request = new Object();
			
			for(x=0;x<formData.length;x++) {
				request[formData[x]['name']] = formData[x]['value'];
			}
			
			var myTr = $("tr[recordID="+request['recordID']+"]");
						
			$("input, button[type=submit]", myTr).attr("disabled", "disabled");
			$("button[type=submit]", myTr).html("<i class='icon-refresh icon-white'></i>");			
		
			$.post("/api.php",
				dataToSend,
				function(data) {
					if(data['status'] == "COMPLETED") {
						$("td[name=name]", myTr).html("<span class='label label-info'>"+request['name']+"</span>."+request['domainName']);
						$("td[name=ttl]", myTr).html(request['ttl']);
						if(request['recordID'].substring(0,2) == "MX")
							$("td[name=data]", myTr).html("<span class='badge badge-info'>"+request['priority']+"</span> &nbsp;"+request['data']);
						else
							$("td[name=data]", myTr).html(request['data']);
						$("td.recordActionsRow", myTr).html("");
						$("td.recordDeleteRow", myTr).html($("td.recordDeleteRow", myTr).attr('oldVal'));
						$("#editForm input[name=recordID]").val("");
						$("td[oldVal]", myTr).removeAttr('oldVal');
						myTr.removeAttr("plugin");
						createEditRecordClickListener();
					}
					else {
						$("td.recordActionsRow", myTr).popover({
							"placement": "top",
							"trigger": "manual",
							"title": "Error",
							"content": "An error has occurred. Your changes have not been saved. Please press Cancel and try again."
						}).popover('show');
						$("button[type=submit]", myTr).removeClass('btn-success').addClass('btn-danger').html("<i class='icon-info-sign icon-white'></i>");
					}
				},
				"json"
			);
		
			return false;
		});
	}
	
	//Add Record Popovers
	$("#addRecordName").popover({
		"placement":"top",
		"trigger": "focus",
		"title": "Record Name",
		"content": "Only leftmost asterisk, alphanumeric characters, periods, and hyphens are valid. Can be left blank for top-level record."
	});
	
	$("#addRecordTTL").popover({
		"placement":"top",
		"trigger": "focus",
		"title": "Time to Live",
		"content": "Value is in seconds. Minimum value is 300 seconds."
	});

	$("#addRecordPriority").popover({
		"placement":"top",
		"trigger": "focus",
		"title": "Priority",
		"content": "Value must be between 0 and 65535."
	});
	
	$("#addRecordType").change(function(){
		if($(this).val() == "MX")
			$("#addRecordPriority").removeAttr('disabled');
		else
			$("#addRecordPriority").attr("disabled", "disabled");
	});
	
	//Add Record Actions
	$("#addForm").submit(function(){
		var myForm = $(this);
		var dataToSend = myForm.serialize();
		var formData = myForm.serializeArray();
		var request = new Object();
		
		for(x=0;x<formData.length;x++) {
			request[formData[x]['name']] = formData[x]['value'];
		}
		
		$("input, select, button.addRecordSubmit", "tfoot").attr("disabled", "disabled");
		$("tfoot button.addRecordSubmit").html('<i class="icon-refresh icon-white"></i>');

		$.post(
			"/api.php",
			dataToSend,
			function(data){
				if(data['status'] == "COMPLETED") {
					if(request['type'] == "MX")
						var priority = "<span class='badge badge-info'>"+request['priority']+"</span> &nbsp;";
					else
						var priority = "";
						
					var newRow = "\
						<tr style='display: none;' recordType='"+request['type']+"'>\
							<td class='editable input-append' name='name'><span class='label label-info'>"+request['name']+"</span>."+request['domainName']+"</td>\
							<td class='editable' name='ttl'>"+request['ttl']+"</td>\
							<td>IN <span class='label'>"+request['type']+"</span></td>\
							<td class='editable' name='data'>"+priority + request['data']+"</td>\
							<td class='recordActionsRow'></td>\
							<td class='recordDeleteRow r2d' style='text-align: center;'></td>\
						</tr>";
					
					$("tfoot input").val("").removeAttr("disabled");
					$("tfoot select").val("A").removeAttr("disabled");
					$("tfoot input[name=priority]").attr('disabled', 'disabled');
					$("tfoot button.addRecordSubmit").html('<i class="icon-plus icon-white"></i>').removeAttr("disabled");
					$("tbody#domainTable").append(newRow);
					$("tbody#domainTable tr:last").fadeIn();
					$("tbody#domainTable tr:last td:last").popover({
						"placement": "left",
						"trigger": "hover",
						"title": "Refresh to Delete",
						"content": "You need to refresh the page to delete this record. Do so by clicking <a href=''>here</a>.",
						"delay": {"hide":1000}
					});
					$("table").trigger('update');
				}
				else {
					$("tfoot td.addRecordSubmit").popover({
						"placement": "top",
						"trigger": "manual",
						"title": "Error",
						"content": "An error has occurred. Your changes have not been saved. Please try again."
					}).popover('show');
					setTimeout(function(){$("tfoot td.addRecordSubmit").popover('hide');}, 3000);
					$("tfoot input").removeAttr("disabled");
					$("tfoot input[name=priority]").attr('disabled', 'disabled');
					$("tfoot select").removeAttr("disabled");
					$("tfoot button.addRecordSubmit").html('<i class="icon-plus icon-white"></i>').removeAttr("disabled");
				}
			},
			"json"
		);
		
		return false;
	});
	
	//Delete Form Actions
	$("thead th:last").click(function(){
		var table = $(this).parent().parent().parent();
		$("input:checkbox", table).attr('checked', 'checked');
	});
	$("#delForm").submit(function(){
		var myForm = $(this);
		var dataToSend = myForm.serialize();
		var formData = myForm.serializeArray();
		var request = new Object();
		
		for(x=0;x<formData.length;x++) {
			request[formData[x]['name']] = formData[x]['value'];
		}
		
		$("tfoot button.delRecordsSubmit").html("<i class='icon-refresh icon-white'></i>").attr("disabled", "disabled");
		$("tbody input:checkbox").attr("disabled", "disabled");
		
		$.post(
			"/api.php",
			dataToSend,
			function(data){
				if(data['status'] == "COMPLETED") {
					$("table input:checked").each(function(){
						$(this).parent().parent().fadeOut().remove();
					});
					$("tfoot button.delRecordsSubmit").html("<i class='icon-trash icon-white'></i>").removeAttr("disabled");
					$("tbody :checkbox[form=delForm]").removeAttr("disabled");
				}
				else { // Report Error
					$("tfoot button.delRecordsSubmit").html("<i class='icon-info-sign icon-white'></i>");
					$("tfoot td.delRecordsSubmit").popover({
						"placement": "left",
						"trigger": "manual",
						"title": "Error",
						"content": "An error has occurred. Some of your changes may have not been saved. Please <a href=''>reload the page</a> and try again."
					}).popover('show');
				}
			},
			"json"
		);
	
		return false;
	});

});