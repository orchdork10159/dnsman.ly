$(function(){

	$("#newAccApi").popover({
		"title": "Rackspace API Key",
		"content": "You can find your API key in the Rackspace Cloud Control Panel under \"Your Account -> API Access\".",
		"placement": "top",
		"trigger": "focus"
	});


	$("#newAcc").submit(function(){
		var myForm = $(this);
		var dataToSend = myForm.serialize();
		
		$("#newAccSubmit").attr("disabled", "disabled").html("<i class='icon-refresh icon-white'></i>");
		$("#newAccName, #newAccUser, #newAccApi").attr("disabled", "disabled");
		
		$.post(
			"api.php",
			dataToSend,
			function(data){
				switch(data['status']) {
					case true:
						$("#newAccSubmit").removeAttr("disabled").html("<i class='icon-plus icon-white'></i>");
						$("#newAccName, #newAccUser, #newAccApi").removeAttr("disabled").val("");
						$("tbody").append("\
						<tr>\
							<td>"+data['name']+"</td>\
							<td>"+data['username']+"</td>\
							<td>\
								<a href='/dashboard/account/"+data['id']+"' class='btn btn-mini btn-primary'>Manage DNS</a>\
							</td>\
						</tr>");
					break;
					default:
				}
			},
			"json"
		);
		
		
		return false;
	});

});