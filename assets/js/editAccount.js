$(function(){

	if(location.hash == "#edit") {
		$("#editAcc").slideDown();
		$("#editAccSummon").parent().addClass('active');
	}

	$("#editAccSummon").click(function(){
		$("#editAcc").slideDown();
		$(this).parent().addClass('active');
	});

	$("#editAccApi").popover({
		"title": "Rackspace API Key",
		"content": "You can find your API key in the Rackspace Cloud Control Panel under \"Your Account -> API Access\".",
		"trigger": "focus"
	});
	
	$("#editAccSubmit").popover({
		"title": "Error",
		"content": "An error ocurred, and your account was not updated. Please try again later.",
		"trigger": "manual"
	});
	
	$("#editAccDelete").popover({
		"title": "Delete Domain",
		"content": "Are you sure you want to delete this account? No changes will be made to the zone file at RSC. Press again to delete. Press cancel to cancel.",
		"trigger": "manual"
	});
	
	function createDelListener() {
		$("#editAccDelete").unbind('click').click(function(){
			$(this).unbind('click');
			$(this).popover('show');
			$(this).click(function(){
				$(this).popover('hide');
				$("input, button", $("#editAcc")).attr('disabled', 'disabled');
				$(this).html("Delete <i class='icon-refresh icon-white'></i>");
				
				$.post(
					"/api.php",
					"action=delAcc&id="+$("#editAccId").val(),
					function(data) {
						switch(data['status']) {
							case "success":
								document.location.href = "/dashboard";
							break;
						}
					},
					"json"
				);
			});
		});
	}
	createDelListener();
	
	$("#editAccCancel").click(function(){
		$("#editAcc").slideUp();
		$("#editAccSummon").parent().removeClass('active');
		$("#editAccDelete").popover('hide').unbind('click');
		createDelListener();
		location.hash = "";
	});
		
	$("#editAccName").keyup(function(){
		$(".accName").text($(this).val());
	});
	
	$("#editAcc").submit(function(){
		var myForm = $(this);
		var dataToSend = myForm.serialize();
		
		$("input, button", myForm).attr('disabled', 'disabled');
		$("#editAccSubmit").html("Update <i class='icon-refresh icon-white'></i>");
		
		$.post(
			"/api.php",
			dataToSend,
			function(data){
				switch(data['status']){
					case "success":
						$("#editAccSubmit").html("Update <i class='icon-ok icon-white'></i>");
						$("#editAcc").append("<br /><br /><div class='alert alert-success'><strong>Success!</strong> Your account has successfully been updated. Click <a href='' class='btn btn-mini btn-success'>here</a> to refresh.</div>");
						//setTimeout(function(){document.location.href="/dashboard/account/"+data['id']+"/";}, 3000);
					break;
					default:
						$("input, button", myForm).removeAttr('disabled');
						$("#editAccSubmit").popover('show');
						setTimeout(function(){$("#editAccSubmit").popover('hide');}, 5000);
						$("#editAccSubmit").html("Update");
				}
			},
			"json"
		);
		
		return false;
	});

});