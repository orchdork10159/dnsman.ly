$(function(){

	$("#newPw").click(function(){
		if($(this).is(":checked"))
			$("#pwFields").slideDown();
		else
			$("#pwFields").slideUp();
			
	});
	
	$("#profileForm").submit(function(){
	
		var myForm = $(this);
		var dataToSend = $(this).serialize();
		var error = false;
		
		$(".error").removeClass("error").find("span").each(function(){$(this).remove();});

		if($("#newPw").is(":checked")) {
			
			if($("#pw").val() != $("#pw2").val())
				error = "match";
			if($("#pw").val().length < 7)
				error = "length";
			
		}
		
		switch(error) {
			case "match":
				$("#pwField2").addClass("error").find(".controls").append("<span class='help-block'>Passowrds to not match!</span>");
			break;
			case "length":
				$("#pwField1").addClass("error").find(".controls").append("<span class='help-block'>Your password must be at least 7 characters.</span>");
			break;
			default:
				$("input, :checkbox, button", myForm).attr("disabled", "disabled");
				$.post(
					"/api.php",
					dataToSend,
					function(data) {
						switch(data['status']) {
							case "email":
								$("#email").append("<span class='help-block'>This email already exists. Try another</span>").parent().addClass("error");
								$("input, :checkbox, button", myForm).removeAttr("disabled");
							break;
							default:
								$("#newPw").removeAttr("checked");
								$("#pwFields").slideUp().find("input").val("");
								$("input, :checkbox, button", myForm).removeAttr("disabled");
								$("#fpwc").slideUp();
								$("#statusMsg").html("<strong>Success!</strong> Your profile has been updated!").slideDown();
								setTimeout(function(){$("#statusMsg").slideUp()}, 5000);
							break;
						}
					},
					"json");
			break;
		}
	
		return false;
	});

});