<? include('h.php'); ?>
<? $plugins = mysql_query("SELECT * FROM `plugins` WHERE `id`='".$_GET['id']."'"); ?>
<? $plugin = mysql_fetch_array($plugins); ?>
	
	<div class="jumbotron">
		<div class="container">
			<h1>Plugin</h1>
			<p class="lead"><?= $plugin['name']; ?></p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="/plugins">Plugins</a> <span class="divider">/</span>
			</li>
			<li class="active">Plugin: <?= $plugin['name']; ?></li>
		</ul>
	
		<div class="row">
			<div class="span3">
				<div class="alert">
					<strong>Double Check!</strong> These plugins include the DNS records recommended by the 3rd party service provider. Please double check your domain's zone to make sure you won't have any conflicting records!
				</div>

				<form class="well form-horizontal" id="installPlugin">
					<h2>Install
						<small>this Plugin</small>
					</h2>
					
					<fieldset class="control-group">
						<label for="accountID">Account:</label>
						<select name="accountID" id="installPluginAccount" style="width: 90%;">
							<option>Select Account...</option>
							<option value="0" hosted="1">DNSMan.ly Hosted DNS (<?= $me->username; ?>)</option>
							<?php
								$accounts = $me->getAccounts();
								foreach($accounts as $acc) {
									echo "
							<option value='".$acc['id']."' hosted='0'>".$acc['name']." (".$acc['apiUsername'].")</option>";
								}
							?>
						</select>
					</fieldset>
	
					<fieldset class="control-group">
						<label for="domainID">Domain:</label>
						<select name="domainID" disabled="disabled" id="installPluginDomain" style="width: 90%;">
							<option value="">Select Domain...</option>
						</select>
					</fieldset>
					
					<input type="hidden" name="action" value="installPlugin" />
					<input type="hidden" name="plugin" value="<?= $plugin['id']; ?>" />
					<input type="hidden" name="domainName" value="" id="installPluginDomainName" />
					
					<button type="submit" class="btn btn-primary" disabled="disabled" id="installPluginSubmit">Install</button>
					
				</form>
			</div>
			<div class="span7">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Name</th>
							<th>TTL</th>
							<th>Type</th>
							<th>Data</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$records = mysql_query("SELECT * FROM `plugin_records` WHERE `plugin`='".$plugin['id']."'");
							while($record = mysql_fetch_array($records)) {
								echo "
						<tr>
							<td><span class='label label-info'>".$record['name']."</span><br class='visible-tablet' />.yourdomain.com</td>
							<td>".$record['ttl']."</td>
							<td>IN <span class='label'>".$record['type']."</span></td>
							<td>";if($record['type']=="MX")echo"<span class='badge badge-info'>".$record['priority']."</span> &nbsp;";echo $record['data']."</td>
						</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
			<div class="span2 hidden-phone">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3331705717583360";
				/* DNSManPro Side */
				google_ad_slot = "9392018718";
				google_ad_width = 120;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>	
	
		</div>

<? $custom['js'][] = "/assets/js/plugin.js"; ?>
<? include('f.php'); ?>