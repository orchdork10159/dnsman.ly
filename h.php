<?php
	define("PATH", "/var/www/vhosts/enge.me/projects/dnsman.ly/repo/");
	require_once(PATH."includes/connect.php");
	require_once(PATH."includes/rackDNS.php");
	require_once(PATH."includes/user.class.php");
	
	if($fli == 1 && !$me->id)
		header("Location: /signup");
	if($fli == -1 && $me->id)
		header("Location: /dashboard");
	if($me->fpwc && $_SERVER['PHP_SELF'] != "/profile.php")
		header("Location: /profile");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>DNSMan.ly | Your DNS Manager for Cloud DNS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Le styles -->
		<style type="text/css">
			body {
				padding-top: 40px;
				padding-bottom: 40px;
			}
		</style>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/responsive.css" rel="stylesheet">
		<link href="/assets/css/custom.css" rel="stylesheet">
		<link href="/assets/css/tablesorter.style.css" rel="stylesheet">
		<link href="/assets/css/jquery.shadow.css" rel="stylesheet">
		<link href="/assets/fancybox/jquery.fancybox.css" rel="stylesheet">
		
		<?php
			if(defined("ISBLOG"))
			echo ISBLOG;
		?>
		
		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=419999748051239";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
		<div class="navbar navbar-fixed-top navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="brand" href="/">DNSMan.ly</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li><a href="/">Home</a></li>
							<li><a href="/plugins">Plugins <i class='icon-th-large icon-white'></i></a></li>
							<li><a href="/blog">Blog</a></li>
							<li><a href="http://support.dnsman.ly">Help</a></li>
						</ul>
						<?php if(!$me->id) { ?>
							<form class="navbar-form pull-right" id="loginForm">
								<input name="email" type="text" class="span2" placeholder="Email">
								<input name="password" type="password" class="span2" placeholder="Password">
								<button type="submit" class="btn">Sign in<i class="icon-chevron-right"></i></button>
							</form>
							<ul class="nav pull-right">
								<li><a href="/signup">Sign Up</a></li>
								<li class="divider-vertical"></li>
							</ul>
						<?php } else { ?>
							<ul class="nav pull-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<img src="http://gravatar.com/avatar/<?= md5($me->email); ?>.png?s=20"> Account
										<b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li><a href="/dashboard"><i class="icon-th-list"></i> Dashboard</a>
										<li><a href="/profile"><i class="icon-user"></i> Update Profile</a>
										<li><a href="/dashboard/account/hosted/"><i class="icon-globe"></i> DNSMan.ly Hosted DNS</a></li>
										<li class="divider"></li>
										<?php
											$accounts = $me->getAccounts();
											foreach($accounts as $acc) {
												$authChk[$acc['id']] = new rackDNS($acc['apiUsername'], $acc['apiKey'], $acc['endpoint']);
												$err = $authChk[$acc['id']]->isAuthenticated() ? "" : "<i class='icon-exclamation-sign'></i>";
												echo "
										<li><a href='/dashboard/account/".$acc['id']."'>".$acc['name']." ".$err."</a></li>";
											}
										?>
									</ul>
								</li>
								<li><a href="#logout" id="logout">Logout</a></li>
							</ul>
						<?php } ?>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
