<? $fli = 1; ?>
<? include('h.php'); ?>
	
	<div class="jumbotron">
		<div class="container">
			<h1>Dashboard</h1>
			<p class="lead">Your one-stop shop for DNS!</p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li class="active">Dashboard</li>
		</ul>
		
		<div class="row">
			<div class="span4">
				<div class="alert">
					<strong>Welcome to your DNSMan.ly Beta Access!</strong> Please keep in mind that our website isn't yet complete! We're going to do our best to get things in full working order soon!
				</div>
				<div class="alert">
					<strong>To use DNSMan.ly</strong> your domain must have its nameservers pointed to the following servers:
					<blockquote style="font-style: italic">ns1.dnsman.ly<br />ns2.dnsman.ly</blockquote>
				</div>
			</div>
			<div class="span8">
				<h2>Accounts</h2>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Name</th>
							<th>Username</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><i class="icon-globe"></i> <b>DNSMan.ly Hosted DNS</b></td>
							<td><b><?= $me->username; ?></b></td>
							<td>
								<a href='/dashboard/account/hosted/' class='btn btn-mini btn-primary btn-block'>Manage DNS</a>
							</td>
						</tr>
						<?php
							$accounts = $me->getAccounts();
							foreach($accounts as $acc) {
								$authChk[$acc['id']] = new rackDNS($acc['apiUsername'], $acc['apiKey'], $acc['endpoint']);
								$err = $authChk[$acc['id']]->isAuthenticated() ? 0 : 1;
								echo "
						<tr>
							<td>".$acc['name']."</td>
							<td>".$acc['apiUsername']."</td>
							<td>";
								if($err)
									echo "
							<a href='/dashboard/account/".$acc['id']."/#edit' class='btn btn-mini btn-danger btn-block'><i class='icon-exclamation-sign icon-white'></i> Invalid API Key</a>";
								else
									echo "
								<a href='/dashboard/account/".$acc['id']."' class='btn btn-mini btn-primary btn-block'>Manage DNS</a>
							</td>
						</tr>";
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td><input style="width: 90%" type="text" placeholder="Account Nickname" name="name" form="newAcc" id="newAccName" required="required" /></td>
							<td class="form-inline">
								<input style="width: 45%" type="text" placeholder="RSC Username" name="username" form="newAcc" id="newAccUser" required="required" />
								<input style="width: 30%" type="password" placeholder="API Key" name="apiKey" form="newAcc" id="newAccApi" required="required" />
							</td>
							<td>
								<button id="newAccSubmit" class="btn btn-primary" form="newAcc"><i class="icon-plus icon-white"></i></button>
							</td>
						</tr>
					</tfoot>
				</table>
				
				<form id="newAcc">
					<input type="hidden" name="action" value="newAcc" />
				</form>
			</div>
		</div>

<? $custom['js'][] = "/assets/js/dashboard.js"; ?>
<? include('f.php'); ?>