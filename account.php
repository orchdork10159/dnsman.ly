<? $fli = 1; ?>
<? include('h.php'); ?>
<? $hosted = $_GET['hosted']; ?>
<? $acc = $me->getAccount($_GET['id'], $hosted) or $die = 1; ?>
<? $api = new rackDNS($acc['apiUsername'], $acc['apiKey'], $acc['endpoint']); ?>

	<div class="jumbotron">
		<div class="container">
			<h1><?= $die ? "You don't belong here." : "Account"; ?></h1>
			<p class="lead"><?= $die ? "" : $acc['name']; ?></p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="/dashboard">Dashboard</a> <span class="divider">/</span>
			</li>
			
			<? if(!$die): ?><li class="active">Account: <? if($hosted): ?><i class='icon-globe'></i> <? endif; ?><span class='accName'><?= $acc['name']; ?></span></li><? endif; ?>
		</ul>
		
		<?php
			if($die) {
				include('f.php');
				die();
			}
		?>
	
		<div class="row">
			<div class="span3">
				<? if(!$hosted): ?>
					<form class="well form-horizontal" id="editAcc" style="display:none;">
					
						<h2>Details
							<small>RSC</small>
						</h2>
						
						<div class="alert alert-error" id="invalidAlert" style="display: none;"><strong>Invalid API Key!</strong> Either your username or API Key is not valid. Please check your username and reenter your API Key.</div>
						
						<fieldset class="control-group">
							<label for="name">Account Nickname:</label>
							<input style="width: 90%" type="text" name="name" required="required" value="<?= $acc['name']; ?>" id="editAccName" />
						</fieldset>
						
						<fieldset class="control-group">
							<label for="apiUsername">RSC Username:</label>
							<input style="width: 90%" type="text" name="apiUsername" required="required" value="<?= $acc['apiUsername']; ?>" />
						</fieldset>
						
						<fieldset class="control-group">
							<label for="apiKey">RSC API Key:</label>
							<input style="width: 90%" type="text" name="apiKey" placeholder="XXXXXXXXXXXXXXXXXXXXXX<?= substr($acc['apiKey'], 28); ?>" id="editAccApi" />
						</fieldset>
						
						<button type="submit" class="btn btn-primary" id="editAccSubmit">Update</button>
						<button type="reset" class="btn btn-warning" id="editAccCancel">Cancel</button>
						<button type="button" class="btn btn-danger" id="editAccDelete">Delete</button>
						
						<input type="hidden" name="action" value="editAcc" />
						<input type="hidden" name="id" value="<?= $acc['id']; ?>" id="editAccId" />
					
					</form>
				<? endif; ?>
	
				<ul class='nav nav-list well'>
					<? if(!$hosted): ?>
						<li class='nav-header'>Account Options</li>
						<li><a href="#edit" id="editAccSummon">Edit Account</a></li>
						<li class="divider"></li>
					<? endif; ?>
					<li><a href="#add">Add Domain</a></li>
				</ul>
			</div>
			<div class="span7">
				<h2>Domains</h2>
				<table class="table table-striped table-bordered table-hover tablesorter sort-domains">
					<thead>
						<tr>
							<th>Domain</th>
							<th width="1"></th>
							<th width="1">Delete</th>
						</tr>
					</thead>
					<tbody id="domainsTable">
						<?php
							$call = $api->list_domains(100);
							$calltwo = $api->list_domains(100,100);
							$domains = array_merge($call['domains'], $calltwo['domains']);
							foreach($domains as $domain) {
								if($hosted == 1) {
									$domainIsMine = mysql_query("SELECT * FROM `hostedDomains` WHERE `domainID`='".$domain['id']."' AND `user`='".$me->id."'");
									$permitDomain = mysql_num_rows($domainIsMine);
									$accUrl = "hosted";
								}
								else {
									$permitDomain = 1;
									$accUrl = $acc['id'];
								}
								if($permitDomain)
									echo "
						<tr>
							<td><a href='/dashboard/account/{$accUrl}/domain/{$domain['id']}'>".$domain['name']."</a></td>
							<td>
								
							</td>
							<td style='text-align: center;'>
								<input type='checkbox' name='domainID[]' value='".$domain['id']."' form='delForm'>
							</td>
						</tr>";
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td>
								<a name="add"></a>
								<input type="text" placeholder="Domain Name" name="name" form="addForm" style="width: 90%" />
							</td>
							<td class="addDomainSubmit"><button type="submit" class="btn btn-primary addDomainSubmit" form="addForm"><i class="icon-plus icon-white"></i></button></td>
							<td class="delDomainSubmit"><button type="submit" class="btn btn-danger delDomainSubmit" form="delForm"><i class="icon-trash icon-white"></i></button></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="span2 hidden-phone">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3331705717583360";
				/* DNSManPro Side */
				google_ad_slot = "9392018718";
				google_ad_width = 120;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>
		</div>
		
		<form id="addForm">
			<input type='hidden' name='action' value='addDomain' />
			<input type='hidden' name='user' value='<?= $me->id; ?>' />
			<input type='hidden' name='accountID' value='<?= $acc['id']; ?>' />
			<input type='hidden' name='email' value='<?= $me->email; ?>' />
			<input type='hidden' name='hosted' value='<?= $hosted; ?>' />
		</form>
		<form id="delForm">
			<input type='hidden' name='action' value='delDomains' />
			<input type='hidden' name='accountID' value='<?= $acc['id']; ?>' />
			<input type='hidden' name='hosted' value='<?= $hosted; ?>' />
		</form>
		<div class="modal-backdrop" style="display: none;"></div>
<? $custom['js'][] = "/assets/js/account.js"; ?>
<? $custom['js'][] = "/assets/js/editAccount.js"; ?>
<? if($api->isAuthenticated() == false) $lockdown = 1; ?>
<? include('f.php'); ?>
