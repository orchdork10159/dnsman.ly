<? $rli = 1; ?>
<? include('h.php'); ?>
<? $acc = $me->getAccount($_GET['id']); ?>

	<h1>Edit Account
		<small class='accName'><?= $acc['name']; ?></small>
	</h1>
	<ul class="breadcrumb">
		<li>
			<a href="/">Home</a> <span class="divider">/</span>
		</li>
		<li>
			<a href="/dashboard">Dashboard</a> <span class="divider">/</span>
		</li>
		<li>
			<a href="/dashboard/account/<?= $acc['id']; ?>/">Account: <span class='accName'><?= $acc['name']; ?></span></a> <span class="divider">/</span>
		</li>
		
		<li class="active">Edit Account</li>
	</ul>
	
	<div class="row">
		<div class="span4 offset4">
			<form class="well form-horizontal" id="editAcc">
			
				<h1>Account Details
					<small>RSC</small>
				</h1>
				
				<fieldset class="control-group">
					<label for="name">Account Nickname:</label>
					<input class="span3" type="text" name="name" required="required" value="<?= $acc['name']; ?>" id="editAccName" />
				</fieldset>
				
				<fieldset class="control-group">
					<label for="apiUsername">RSC Username:</label>
					<input class="span3" type="text" name="apiUsername" required="required" value="<?= $acc['apiUsername']; ?>" />
				</fieldset>
				
				<fieldset class="control-group">
					<label for="apiKey">RSC API Key:</label>
					<input class="span3" type="text" name="apiKey" placeholder="XXXXXXXXXXXXXXXXXXXXXXXXXXXX<?= substr($acc['apiKey'], 28); ?>" id="editAccApi" />
				</fieldset>
				
				<button type="submit" class="btn btn-primary" id="editAccSubmit">Update Account</button>
				
				<input type="hidden" name="action" value="editAcc" />
				<input type="hidden" name="id" value="<?= $acc['id']; ?>" />
			
			</form>
		</div>
	</div>


<? $custom['js'][] = "/assets/js/editAccount.js"; ?>
<? include('f.php'); ?>