<? $fli = 1; ?>
<? include('h.php'); ?>
<? $hosted = $_GET['hosted']; ?>
<? $acc = $me->getAccount($_GET['acc'], $hosted) or $die=1; ?>
<? $api = new rackDNS($acc['apiUsername'], $acc['apiKey'], $acc['endpoint']); ?>
<? $dom = $api->list_domain_details($_GET['id']); ?>
<?php
	if($hosted) {
		
		$domainIsMine = mysql_query("SELECT * FROM `hostedDomains` WHERE `domainID`='".$dom['id']."' AND `user`='".$me->id."'");
		$die = mysql_num_rows($domainIsMine) ? 0 : 1;
	}
?>

	<div class="jumbotron">
		<div class="container">
			<h1><?= $die ? "You don't belong here" : "Domain"; ?></h1>
			<p class="lead"><?= $die ? "" : $dom['name']; ?></p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="/dashboard">Dashboard</a> <span class="divider">/</span>
			</li>
			<li>
				<?php
					if($hosted)
						$accUrl = "hosted";
					else
						$accUrl = $acc['id'];
				?>
				<a href="/dashboard/account/<?= $accUrl; ?>/">Account: <?= $acc['name']; ?></a> <span class="divider">/</span>
			</li>
			<? if(!$die): ?><li class="active">Domain: <?= $dom['name']; ?></li><? endif; ?>
		</ul>
		
		<?php
			if($die) {
				include('f.php');
				die();
			}
		?>
		
		<? if($_GET['pluginInstall']): ?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Double Check!!</strong> Your plugin has been installed. Please double check to see if there are any contradictions in your DNS configuration now that the new records have been added.
		</div>
		<? endif; ?>
		
		<div class="alert">
			<button type="button" class="close" data-dismiss="alert">x</button>
			<strong>Nameservers!</strong> Are your nameservers on this domain set to <i>ns1.dnsman.ly</i>, and <i>ns2.dnsman.ly</i>?
		</div>
		
		<h2>All Records
			<small>Click a record to edit. Click a header to sort. Scroll to bottom to add records.</small>
		</h2>
		<table class="table table-striped table-bordered table-hover tablesorter sort-records">
			<thead>
				<tr>
					<th>Name</th>
					<th>TTL</th>
					<th>Type</th>
					<th>Data</th>
					<th style="text-align: center;" width="1"><i class='icon-tag'></i></th>
					<th width="1">Delete</th>
				</tr>
			</thead>
			<tbody id="domainTable" domainName="<?= $dom['name']; ?>">
				<?php
					$call = $api->list_records($dom['id']);
					foreach($call['records'] as $record) {
						
						$recordName = explode(".", $record['name']);
						if(count($recordName) == 2)
							$recordNameField = "<span class='label label-info'></span>.".$record['name'];
						else {
							$recordNameField = "<span class='label label-info'>";
							for($i=0;$i<count($recordName)-2;$i++)
								$recordNameField .= $recordName[$i].".";
							$recordNameField = substr($recordNameField, 0, -1);
							$recordNameField .= "</span>.".$recordName[count($recordName)-2].".".$recordName[count($recordName)-1];
						}
					
					
						if($record['type'] != "SRV") {echo "
				<tr recordID='".$record['id']."' recordType='".$record['type']."'";if(substr($record['comment'],0,6)=="plugin")echo " plugin='".substr($record['comment'],7)."'";echo ">
					<td class='editable input-append' name='name'>".$recordNameField."</td>
					<td class='editable' name='ttl'>".$record['ttl']."</td>
					<td>IN <span class='label'>".$record['type']."</span></td>
					<td class='editable' name='data'>";if($record['type']=="MX")echo"<span class='badge badge-info'>".$record['priority']."</span> &nbsp;";echo $record['data']."</td>
					<td style='text-align: center;' class='recordActionsRow'>";if(substr($record['comment'],0,6)=="plugin")echo " <button class='btn btn-small btn-success selectPlugin' plugin='".substr($record['comment'],7)."'><i class='icon-th-large icon-white'></i></button>";echo "</td>
					<td class='recordDeleteRow' style='text-align: center;'><input type='checkbox' name='recordID[]' value='".$record['id']."' form='delForm' /></td>
				</tr>";}
					}
				?>
			</tbody>
			<tfoot>
				<tr>
					<td class="input-append"><input type="text" placeholder="Record Name" id="addRecordName" name="name" form="addForm" /><span class="add-on">.<?= $dom['name']; ?></span></td>
					<td><input class="input-mini" type="number" placeholder="TTL" min="300" id="addRecordTTL" name="ttl" form="addForm" /></td>
					<td>
						<select class="input-mini" id="addRecordType" form="addForm" name="type">
							<option value="A">A</option>
							<option value="AAAA">AAAA</option>
							<option value="CNAME">CNAME</option>
							<option value="MX">MX</option>
							<option value="NS">NS</option>
							<option value="TXT">TXT</option>
						</select>
					</td>
					<td>
						<input type="number" placeholder="Priority" class="input-mini" disabled="disabled" min="0" max="65535" id="addRecordPriority" name="priority" form="addForm" />
						<input type="text" placeholder="Data" name="data" form="addForm" class="span2" />
					</td>
					<td class="addRecordSubmit"><button class="btn btn-primary addRecordSubmit" type="submit" form="addForm"><i class="icon-plus icon-white"></i></button></td>
					<td class="delRecordsSubmit"><button class="btn btn-danger delRecordsSubmit" type="submit" form="delForm"><i class="icon-trash icon-white"></i></button></td>
				</tr>
			</tfoot>
		</table>
		
		<form id="editForm">
			<input type="hidden" name="action" value="editRecord" />
			<input type="hidden" name="recordID" value="" />
			<input type="hidden" name="domainID" value="<?= $dom['id']; ?>" />
			<input type="hidden" name="domainName" value="<?= $dom['name']; ?>" />
			<input type="hidden" name="accountID" value="<?= $acc['id']; ?>" />
			<input type="hidden" name="hosted" value="<?= $hosted; ?>" />
		</form>
		<form id="addForm">
			<input type="hidden" name="action" value="addRecord" />
			<input type="hidden" name="domainID" value="<?= $dom['id']; ?>" />
			<input type="hidden" name="domainName" value="<?= $dom['name']; ?>" />
			<input type="hidden" name="accountID" value="<?= $acc['id']; ?>" />
			<input type="hidden" name="hosted" value="<?= $hosted; ?>" />
		</form>
		<form id="delForm">
			<input type="hidden" name="action" value="delRecords" />
			<input type="hidden" name="domainID" value="<?= $dom['id']; ?>" />
			<input type="hidden" name="domainName" value="<?= $dom['name']; ?>" />
			<input type="hidden" name="accountID" value="<?= $acc['id']; ?>" />
			<input type="hidden" name="hosted" value="<?= $hosted; ?>" />
		</form>
	
<? $custom['js'][] = "/assets/js/domain.js"; ?>
<? include('f.php'); ?>