<? include('h.php'); ?>
	<div class="jumbotron home">
		<div class="container">
			<h1>DNSMan.ly</h1>
			<h2>The best DNS interface ever!</h2>
			<p>Technical configuration doesn't have to be ugly... Use DNSMan.ly to manage your <a href="http://rackspace.com" target="_blank">Rackspace</a> Cloud accounts or host your DNS for free!</p>
		</div>
	</div>
	
	<div class="container">
	
		<div class="row-fluid">
			<div class="span4 well">
				<h3>Quick DNS Management</h3>
				<p>By using the best DNS interface around, you can quickly and easily update, create, and delete DNS records on the fly for multiple domains. Inline-editing means all of your changes can be made on one page! No more reloading multiple pages to do a simple DNS change. </p>
				<p><a class="btn" href="/signup">View details &raquo;</a></p>
			</div>
			<div class="span4 well">
				<h3>Rackspace or Hosted</h3>
				<p>Have <a href="http://rackspace.com" target="_blank">Rackspace</a> <a href="http://rackspace.com/cloud/cloud_hosting_products/servers/" target="_blank">Cloud Servers</a>? Then you can quickly access and manage all of your <a href="http://rackspace.com" target="_blank">Rackspace</a> domains with DNSMan.ly's intuitive interface to get the job done. If you don't have <a href="http://rackspace.com" target="_blank">Rackspace</a>, simply host your DNS with DNSMan.ly by pointing your domain's nameservers to us!  </p>
				<p><a class="btn" href="/signup">View details &raquo;</a></p>
			</div>
			<div class="span4 well">
				<h3>Quickly Install Plugins</h3>
				<p>Using your domain name with a popular service like Tumblr or Google Apps? Browse our plugins library to see how easy it is to install these apps on your domain! Install the plugin, then customize to your needs with our awesome interface. Using your domain to your advantage has never been easier!</p>
				<p><a class="btn" href="/plugins">Browse Plugins &raquo;</a></p>
			</div>
		</div>
	
<? $noad = 1; ?>
<? include('f.php'); ?>