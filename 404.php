<!DOCTYPE html>
<html>
<head>
	<title>DNSMan.ly | Page Not Found</title>
	<style type="text/css">
		* {
			padding: 0;
			margin: 0;
		}
		html {
			height: 100%;
		}
		body {
			height: 100%;
			background-image: url('/assets/img/404.jpg');
			background-size: auto;
			background-position: center center;	
			background-repeat: no-repeat;
		}
		#error {
			position: relative;
			top: 75%;
			width: 100%;
			text-align: center;
		}
		#error h1 {
			color: #EEE;
			text-shadow: 0px 1px 4px black;
			font-size: 30px;
			letter-spacing: -1px;
			font-family: Arial,'Helvetica Neue',Helvetica,sans-serif;
		}
		#error p {
			color: #243748;
			text-shadow: 0px 1px 1px white;
			font-family: Arial,'Helvetica Neue',Helvetica,sans-serif;
			font-size: 18px;
			line-height: 24px;
			margin-top: 10px;
		}
	</style>
	<meta charset="utf-8" />
 </head>
<body>
	<div id="error">
		<h1>Oops, our bad!</h1>
		<p>Sorry, but the page you're looking for cannot be found.<br />Please go back and try again.</p>
	</div>
</body>
</html>