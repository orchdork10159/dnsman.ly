<?php
	define("PATH", "/var/www/vhosts/enge.me/projects/dnsman.ly/repo/");
	require_once(PATH."includes/connect.php");
	require_once(PATH."includes/rackDNS.php");
	require_once(PATH."includes/user.class.php");
	require_once(PATH."includes/MCAPI.class.php");
	
	$acc = $me->getAccount($_POST['accountID'], $_POST['hosted']);
	$api = new rackDNS($acc['apiUsername'], $acc['apiKey'], $acc['endpoint']);
	$mc = new MCAPI(MCAPIKEY);
	
	if($_POST['hosted']) {
		$domainIsMine = mysql_query("SELECT * FROM `hostedDomains` WHERE `domainID`='".$_POST['domainID']."' AND `user`='".$me->id."'");
		$die = mysql_num_rows($domainIsMine) ? 0 : 1;
	}
	if(!$die)
		$dom = $api->list_domain_details($_POST['domainID']);
			
	switch($_POST['action']) {
		case "signup":
			$emails = mysql_query("SELECT * FROM `users` WHERE `email`='".$_POST['email']."'");
			
			if(mysql_num_rows($emails) > 0) {
				$return['status'] = "email";
				break;
			}
			
			$activation = rand(10000,99999);
		
			$sql = "INSERT INTO `users` (`username`, `email`, `password`, `dateCreated`, `activation`) VALUES ('".$_POST['username']."', '".$_POST['email']."', MD5('AaZz".$_POST['pw']."'), CURRENT_TIMESTAMP, '".$activation."')";
			$query = mysql_query($sql);
			if($query) {
				$sub = $mc->listSubscribe(MCLISTID, $_POST['email'], array("name" => $_POST['username'], "activation" => $activation, "active" => "0"), 'html', false, false, true, true);
				if($sub)
					$return['status'] = "success";
				else
					$return['status'] = $mc->errorMessage;
			}
			else
				$return['status'] = mysql_error();
		break;
		case "profile":
			$emails = mysql_query("SELECT * FROM `users` WHERE `email`='".$_POST['email']."' AND `id`!='".$me->id."'");
			
			if(mysql_num_rows($emails) > 0) {
				$return['status'] = "email";
				break;
			}

			$mc->listUpdateMember(MCLISTID, $me->email, array('email' => $_POST['email'], 'name' => $_POST['username']));
			$return = $me->updateProfile($_POST['username'], $_POST['email'], $_POST['newPw'], $_POST['pw'], $_POST['pw2']);
		break;
		case "newAcc":
			$return = $me->newAcc($_POST);
		break;
		case "editAcc":
			if($_POST['id'] == 0)
				break;
			$return = $me->editAcc($_POST);
		break;
		case "delAcc":
			if($_POST['id'] == 0)
				break;
			$return = $me->delAcc($_POST);
		break;
		case "addRecord":
			if($_POST['name'] == "")
				$recordName = $dom['name'];
			else
				$recordName = $_POST['name'].".".$dom['name'];
			
			if($_POST['priority'] == "")
				$_POST['priority'] = false;
			
			$records = array($api->create_domain_record_helper($_POST['type'],$recordName,$_POST['data'],$_POST['ttl'],$_POST['priority']));
			$return = $api->create_domain_record($dom['id'], $records);
		break;
		case "editRecord":
			if($_POST['name'] == "")
				$recordName = $dom['name'];
			else
				$recordName = $_POST['name'].".".$dom['name'];
				
			$return = $api->modify_record($dom['id'], $_POST['recordID'], $_POST['ttl'], $recordName, $_POST['priority'], $_POST['data']);
		break;
		case "delRecords":
			$return = $api->delete_domain_records($dom['id'],$_POST['recordID']);
		break;
		case "installPlugin":
			if(!$_POST['plugin'] && !$dom['id'] && !$dom['name'])
				break;
			
			$plugins = mysql_query("SELECT * FROM `plugins` WHERE `id`='".$_POST['plugin']."' AND `active`=1");
			$plugin = mysql_fetch_array($plugins);
			
			$pluginRecords = mysql_query("SELECT * FROM `plugin_records` WHERE `plugin`='".$plugin['id']."'");
			while($record = mysql_fetch_array($pluginRecords)) {
				if($record['name'] == "")
					$recordName = $dom['name'];
				else
					$recordName = $record['name'].".".$dom['name'];
				$records[] = $api->create_domain_record_helper($record['type'],$recordName,$record['data'],$record['ttl'],$record['priority'],"plugin:".$_POST['plugin']);
			}		
			
			$return = $api->create_domain_record($dom['id'], $records);
		break;
		case "getDomains":
			$options = "<option value=''>Select Domain...</option>";
			
			$call = $api->list_domains(99);
			foreach($call['domains'] as $domain) {
				if($_POST['hosted']) {
					$domainIsMineA = mysql_query("SELECT * FROM `hostedDomains` WHERE `domainID`='".$domain['id']."' AND `user`='".$me->id."'");
					if(mysql_num_rows($domainIsMineA))
						$options .= "<option value='".$domain['id']."'>".$domain['name']."</option>";
				} else
					$options .= "<option value='".$domain['id']."'>".$domain['name']."</option>";
			}
			
			$return['status'] = "success";
			$return['html'] = $options;
			$return['account'] = $acc['id'];
		break;
		case "addDomain":
			$return = $api->create_domain($_POST['name'], $_POST['email'], 300);
			if($return['status'] == "COMPLETED") {
				$addedDomainId = $api->get_domain_id($_POST['name']);
				$return['newId'] = $addedDomainId;
				if($_POST['hosted'] == 1)
					$me->addDomain($addedDomainId, $_POST['name']);
			}
		break;
		case "delDomains":
			//Overwriting $dom and $domainIsMinefor and $die each domain ID
			$die = 0;
			if($_POST['hosted']) {
				foreach($_POST['domainID'] as $domId) {
					$domainIsMine = mysql_query("SELECT * FROM `hostedDomains` WHERE `domainID`='".$domId."' AND `user`='".$me->id."'");
					if(!mysql_num_rows($domainIsMine))
						$die = 1;
				}
			}
			if(!$die){
				if($_POST['hosted'] == 1)
					$me->deleteDomains($_POST['domainID']);
				$return = $api->delete_domains($_POST['domainID']);
			}
		break;
	}
	$return['response'] = $api->getLastResponseStatus() . ": " .$api->getLastResponseMessage();
	$return['domain'] = $dom;
	echo json_encode($return);
?>