<? $fli = -1; ?>
<? include('h.php'); ?>

	<div class="jumbotron">
		<div class="container">
			<h1>Signup</h1>
			<p class="lead">Be among the best interfaces for DNS Management today!</p>
		</div>
	</div>
	
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3">
				<div class="well">
					<form class="form-horizontal" id="signup">
						<div id="signupFormContents">
						
							<legend>Signup for DNSMan.ly <span class="label label-info pull-right">BETA</span></legend>
		
							<div class="control-group">
								<label class="control-label" for="username">Full Name:</label>
								<div class="controls">
									<input type="text" name="username" required="required" id="signupUsername" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="email">Email:</label>
								<div class="controls">
									<input type="email" name="email" required="required" id="signupEmail" />
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Avatar:</label>
								<div class="controls">
									<img src="http://gravatar.com/avatar/123.png?s=30" id="signupAvatar" />
									<span class="help-inline">Update your Avatar at <a href="http://gravatar.com" target="_blank">Gravatar.com</a></span>
								</div>
							</div>
							
							<div class="control-group" id="pwField1">
								<label class="control-label">Password:</label>
								<div class="controls">
									<input type="password" name="pw" required="required" id="pw" />
								</div>
							</div>
							
							<div class="control-group" id="pwField2">
								<label class="control-label">Repeat:</label>
								<div class="controls">
									<input type="password" name="pw2" required="required" id="pw2" />
								</div>
							</div>
							
							<input type="hidden" name="action" value="signup" />
							
							<div class="alert alert-error" style="display: none;" id="signupError">
								<strong>Uh oh!</strong> For some reason, your beta request has failed. We're looking into the problem. Try again later!
							</div>
							
							<button type="submit" class="btn btn-primary btn-block" id="signupSubmit">Sign Up</button>
							
						</div>
						
						<div class="alert alert-success" style="display: none;" id="signupSuccess">
							<strong>Great!</strong> Check your email to activate your account!</div>
		
					</form>
				
				</div>
			</div>
		</div>

<? $noad = 1; ?>
<? $custom['js'][] = "/assets/js/jquery.md5.js"; ?>
<? $custom['js'][] = "/assets/js/signup.js"; ?>
<? include('f.php'); ?>