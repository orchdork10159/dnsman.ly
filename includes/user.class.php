<?php

session_start();

class User {

	public $id;
	public $username;
	public $email;
	public $password;
	public $fpwc;
	public $dateCreated;
	private $apiUsername;
	private $apiKey; 
	
	function __construct($e, $p) {
		
		$users = mysql_query("SELECT * FROM `users` WHERE `email`='".$e."' AND `password`='".$p."' AND `active`=1");
		$user = mysql_fetch_array($users);
		
		if(!mysql_num_rows($users))
			return false;
		
		$this->id = $user['id'];
		$this->username = $user['username'];
		$this->email = $user['email'];
		$this->password = $user['password'];
		$this->fpwc = $user['fpwc'];
		$this->dateCreated = $user['dateCreated'];
		
	}
	
	function updateProfile($username, $email, $newPw, $pw, $again) {
		$sql = "UPDATE `users` SET `username`='".$username."', `email`='".$email."'";
		if($newPw && $pw == $again) {
			$sql .= ", `password`=MD5('AaZz".$pw."'), `fpwc`=0";
			$_SESSION['password'] = MD5('AaZz'.$pw);
		}
		$sql .= " WHERE `id`='".$this->id."'";
		$_SESSION['username'] = $username;
		$_SESSION['email'] = $email;
			
		return mysql_query($sql);
	}
	
	function recordLog($action) {
		mysql_query("INSERT INTO `logins` (`user`, `time`, `action`, `realm`) VALUES ('".$this->id."', NOW(), '".$action."', '".$_SERVER['HTTP_HOST']."')");
	}
	
	function getAccount($id,$hosted = 0) {
	
		if(!is_numeric($id))
			return;
		
		if($hosted == 1)
			$accounts = mysql_query("SELECT * FROM `apiAccounts` WHERE `id`='".$id."'");
		else
			$accounts = mysql_query("SELECT * FROM `apiAccounts` WHERE `user`='{$this->id}' AND `id`='".$id."'");
		return mysql_fetch_array($accounts);
		
	}
	
	function getAccounts() {
		
		$accounts = mysql_query("SELECT * FROM `apiAccounts` WHERE `user`='{$this->id}'");
		while($acc = mysql_fetch_array($accounts)) {
			$return[] = $acc;
		}
		return $return;
	}
	
	function editAcc($var) {
		$acc = $this->getAccount($var['id']);
		if($var['apiKey'] == "")
			$var['apiKey'] = $acc['apiKey'];
		
		$return['status'] = mysql_query("UPDATE `apiAccounts` SET `name`='".str_replace("'", "\'", $var['name'])."', `apiUsername`='".$var['apiUsername']."', `apiKey`='".$var['apiKey']."' WHERE `id`='".$acc['id']."'") ? "success" : "error";
		$return['id'] = $acc['id'];
		
		return $return;
	}
	
	function delAcc($var) {
		$acc = $this->getAccount($var['id']);
		
		if(!$acc['id'])
			return array("status" => false);
		
		$return['status'] = mysql_query("DELETE FROM `apiAccounts` WHERE `id`='".$acc['id']."'") ? "success" : "error";
		
		return $return;
	}
	
	function newAcc($var) {
		$return['status'] = mysql_query("INSERT INTO `apiAccounts` (`name`, `user`, `apiUsername`, `apiKey`) VALUES ('".$var['name']."', '".$this->id."', '".$var['username']."', '".$var['apiKey']."')");
		$return['id'] = mysql_insert_id();
		foreach($var as $key=>$value)
			$return[$key] = $value;
		
		return $return;
	}
	
	function addDomain($domainID, $domainName) {
		return mysql_query("INSERT INTO `hostedDomains` (`user`, `domainID`, `domainName`) VALUES ('".$this->id."', '".$domainID."', '".$domainName."')");
	}
	
	function deleteDomains($domainID) {
		$query = "DELETE FROM `hostedDomains` WHERE";
		foreach($domainID as $domain)
			$query .= " `domainId`='".$domain."' OR";
		return mysql_query(substr($query, 0, -2));
	}
	
}

$me = new User($_SESSION['email'], $_SESSION['password']);

?>