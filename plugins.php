<? $fli = 1; ?>
<? include('h.php'); ?>

	<div class="jumbotron">
		<div class="container">
			<h1>Plugins</h1>
			<p class="lead">Quick installs of your favorite apps!</p>
		</div>
	</div>
	
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li class="active">Plugins</li>
		</ul>
	
		<div class="row">
			<div class="span3">
				<div class="alert">
					<strong>Double Check!</strong> These plugins include the DNS records recommended by the 3rd party service provider. Please double check your domain's zone to make sure you won't have any conflicting records!
				</div>
			</div>
			<div class="span7">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Name</th>
						</tr>
					</thead>
					<tbody class="tooltips">
						<?php
							$plugins = mysql_query("SELECT * FROM `plugins` WHERE `active`=1");
							while($plugin = mysql_fetch_array($plugins)) {
								echo "
						<tr title='".$plugin['description']."'>
							<td><a href='/plugins/".$plugin['id']."' name='".$plugin['name']."'>".$plugin['name']."</a></td>
						</tr>";
								
							}
						?>
					</tbody>
				</table>
			</div>
			<div class="span2 hidden-phone">
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3331705717583360";
				/* DNSManPro Side */
				google_ad_slot = "9392018718";
				google_ad_width = 120;
				google_ad_height = 600;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>	
		</div>
<? $custom['js'][] = "/assets/js/plugins.js"; ?>
<? include('f.php'); ?>